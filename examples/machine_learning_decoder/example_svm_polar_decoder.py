import numpy as np
import matplotlib.pyplot as plt

from digcommpy import messages
from digcommpy import encoders
from digcommpy import channels
from digcommpy import modulators
from digcommpy import decoders
from digcommpy import metrics

def main(n, k):
    snrs = np.arange(-5, 6)
    ber = {"ref": [], "ml": []}
    mess = messages.generate_data(k, number=5000, binary=True)
    for snr_db in snrs:
        print("Simulating SNR={:.2f}dB".format(snr_db))
        enc = encoders.PolarEncoder(n, k, 'BAWGN', snr_db)
        mod = modulators.BpskModulator()
        cha = channels.BawgnChannel(snr_db, rate=k/n, input_power=1.)
        dec_ref = decoders.PolarDecoder(n, k, 'BAWGN', snr_db)
        train_info = messages.generate_data(k, binary=True)
        train_code = enc.encode_messages(train_info)
        train_code = mod.modulate_symbols(train_code)
        dec_ml = decoders.SvmDecoder(n, k)
        dec_ml.train_system((train_code, train_info))
        code = enc.encode_messages(mess)
        code = mod.modulate_symbols(code)
        rec = cha.transmit_data(code)
        est_ref = dec_ref.decode_messages(rec, cha)
        est_ml = dec_ml.decode_messages(rec)
        ber['ref'].append(metrics.ber(mess, est_ref))
        ber['ml'].append(metrics.ber(mess, est_ml))
    plt.semilogy(snrs, ber['ref'], 'o-', label='Reference')
    plt.semilogy(snrs, ber['ml'], 'o-', label='Machine Learning')
    plt.xlabel("SNR/dB")
    plt.ylabel("BER")
    plt.title("Polar Code N={}, k={} for different Binary-Input-AWGN Channels"
              .format(n, k))
    plt.legend()

if __name__ == "__main__":
    n, k = 16, 5
    main(n, k)
    plt.show()
