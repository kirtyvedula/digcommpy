import numpy as np
import matplotlib.pyplot as plt

from digcommpy import messages
from digcommpy import encoders
from digcommpy import channels
from digcommpy import modulators
from digcommpy import decoders
from digcommpy import metrics

def main(n, k):
    snrs = np.arange(-5, 6)
    ber = []
    mess = messages.generate_data(k, number=1000, binary=True)
    for snr_db in snrs:
        print("Simulating SNR={:.2f}dB".format(snr_db))
        enc = encoders.PolarEncoder(n, k, 'BAWGN', snr_db)
        mod = modulators.BpskModulator()
        cha = channels.BawgnChannel(snr_db, rate=k/n, input_power=1.)
        dec = decoders.PolarDecoder(n, k, 'BAWGN', snr_db)
        code = enc.encode_messages(mess)
        code = mod.modulate_symbols(code)
        rec = cha.transmit_data(code)
        est = dec.decode_messages(rec, cha)
        ber.append(metrics.ber(mess, est))
    plt.semilogy(snrs, ber, 'o-')
    plt.xlabel("SNR/dB")
    plt.ylabel("BER")
    plt.title("Polar Code N={}, k={} for different Binary-Input-AWGN Channels"
              .format(n, k))

if __name__ == "__main__":
    n, k = 16, 5
    main(n, k)
    plt.show()
