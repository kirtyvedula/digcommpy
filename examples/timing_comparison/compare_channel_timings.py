import time

from digcommpy import messages
from digcommpy import channels

def main(info_length=8, number=1000000):
    mess = messages.generate_data(info_length, number=number, binary=True)
    _channels = {"bsc": channels.BscChannel(.2),
                 "bec": channels.BecChannel(.3),
                 "bawgn": channels.BawgnChannel(0),
                }
    print("Comparing different channel speeds.")
    print("Testing {:.2E} messages with k={}".format(number, info_length))
    print("{:10}\tTime (s)\n{}".format("Channel", "-"*25))
    for name, channel in _channels.items():
        time_start = time.time()
        output = channel.transmit_data(mess)
        time_end = time.time()
        print("{:10}\t{:.3f}".format(name, time_end-time_start))

if __name__ == "__main__":
    main()
