import time

from digcommpy import messages
from digcommpy import encoders

def main(code_length=32, info_length=8, number=1000000):
    mess = messages.generate_data(info_length, number=number, binary=True)
    codes = {"identity": encoders.IdentityEncoder(code_length, info_length),
             "repetition": encoders.RepetitionEncoder(code_length),
             #"codebook": encoders.CodebookEncoder(code_length, info_length, codebook),
             "polar": encoders.PolarEncoder(code_length, info_length, "BAWGN"),
             "polar_wt": encoders.PolarWiretapEncoder(code_length, "BAWGN", "BAWGN", 0, -5),
            }
    print("Comparing different encoder speeds.")
    print("Encoding {:.2E} messages with n={}, k={}".format(number, code_length, info_length))
    print("{:10}\tTime (s)\n{}".format("Encoder", "-"*25))
    for name, encoder in codes.items():
        time_start = time.time()
        codewords = encoder.encode_messages(mess)
        time_end = time.time()
        print("{:10}\t{:.3f}".format(name, time_end-time_start))

if __name__ == "__main__":
    main()
