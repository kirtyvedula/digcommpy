import numpy as np
from digcommpy import parsers

EXAMPLE_CODEBOOK = "example_codebook.csv"
EXAMPLE_WIRETAP_CODEBOOK = "example_codebook_wiretap.csv"
EXAMPLE_HYPERPARAM_RESULTS = "hyperparam_simulation_results.res"

def test_parse_codebook():
    codebook, code_info = parsers.read_codebook_file(EXAMPLE_CODEBOOK)
    expected = {0: [1, 2, 3, 4, 5], 1: [-1, -2, -3, -4, -5],
                2: [0, 0, 0, 0, 0], 3: [.5, -.5, .5, -.5, 0]}
    expected_info = {'code_length': 5, 'info_length': 2}
    assert (codebook == expected) and (code_info == expected_info)

def test_parse_wiretap_codebook():
    codebook, code_info = parsers.read_codebook_file(EXAMPLE_WIRETAP_CODEBOOK,
                                                     wiretap=True)
    expected = {0: [1, 2, 3, 4, 5], 1: [5, 4, 3, 2, 1], 2: [-1, -2, -3, -4, -5],
                3: [-5, -4, -3, -2, -1], 4: [0, 0, 0, 0, 0], 5: [1, 1, 1, 1, 1],
                6: [.5, -.5, .5, -.5, 0], 7: [0, -.5, .5, -.5, .5]}
    expected_info = {'code_length': 5, 'info_length': 2, 'random_length': 1}
    assert (codebook == expected) and (code_info == expected_info)

def test_parse_hyperparameter_search_results():
    constants, results = parsers.read_hyperparameter_search_results(
        EXAMPLE_HYPERPARAM_RESULTS)
    exp_constants = {'code_length': 16, 'info_length': 4, 'random_length': 3, 'test_size': 1000}
    exp_results = [({'layer': [16, 8], 'train_snr': -5}, {(('snr_db', -5),): {'ber': 0.497, 'bler': 0.936}, (('snr_db', 0),): {'ber': 0.48875, 'bler': 0.941}, (('snr_db', 5),): {'ber': 0.49575, 'bler': 0.941}}),
                   ({'layer': [16, 8], 'train_snr': 5}, {(('snr_db', -5),): {'ber': 0.49475, 'bler': 0.925}, (('snr_db', 0),): {'ber': 0.49125, 'bler': 0.925}, (('snr_db', 5),): {'ber': 0.48725, 'bler': 0.932}}),
                   ({'layer': [128], 'train_snr': -5}, {(('snr_db', -5),): {'ber': 0.51, 'bler': 0.956}, (('snr_db', 0),): {'ber': 0.508, 'bler': 0.959}, (('snr_db', 5),): {'ber': 0.496, 'bler': 0.961}}),
                   ({'layer': [128], 'train_snr': 5}, {(('snr_db', -5),): {'ber': 0.50675, 'bler': 0.964}, (('snr_db', 0),): {'ber': 0.5075, 'bler': 0.967}, (('snr_db', 5),): {'ber': 0.506, 'bler': 0.972}})]
    assert (constants == exp_constants) and (results == exp_results)

def test_convert_codebook_to_dict():
    messages = np.array([[0, 1], [1, 0], [1, 1], [0, 0]])
    codewords = np.array([[1, 2], [3, 4], [5, 6], [7, 8]])
    exp_constants = {'code_length': 2, 'info_length': 2}
    exp_results = {0: [7, 8], 1: [1, 2], 2: [3, 4], 3: [5, 6]}
    codebook, code_info = parsers.convert_codebook_to_dict(messages, codewords)
    _compare_codebook = [np.all(np.array(v) == codebook[k]) for k, v in exp_results.items()]
    _compare_codebook = all(_compare_codebook) and (exp_results.keys() == codebook.keys())
    assert _compare_codebook and (code_info == exp_constants)

def test_convert_codebook_to_dict_wiretap():
    messages = np.array([[0, 1], [1, 0], [1, 1], [0, 0], [0, 1], [1, 1], [1, 0],
                         [0, 0]])
    random = np.array([[0], [0], [1], [0], [1], [0], [1], [1]])
    codewords = np.array([[1, 2], [3, 4], [5, 6], [7, 8], [9, 0], [-1, -2],
                          [-3, -4], [-5, -6]])
    exp_constants = {'code_length': 2, 'info_length': 2, 'random_length': 1}
    exp_results = {2: [1, 2], 4: [3, 4], 7: [5, 6], 0: [7, 8], 3: [9, 0],
                   6: [-1, -2], 5: [-3, -4], 1: [-5, -6]}
    codebook, code_info = parsers.convert_codebook_to_dict(messages, codewords, random)
    _compare_codebook = [np.all(np.array(v) == codebook[k]) for k, v in exp_results.items()]
    _compare_codebook = all(_compare_codebook) and (exp_results.keys() == codebook.keys())
    assert _compare_codebook and (code_info == exp_constants)
