import pytest

from digcommpy import encoders
from digcommpy import decoders
from digcommpy import channels
from digcommpy import modulators
from digcommpy import simulations

@pytest.mark.parametrize("channel", [channels.BecChannel, channels.BscChannel])
def test_simulation_binary(channel):
    code_opt = {"code_length": 3, "info_length": 3}
    encoder = encoders.IdentityEncoder
    decoder = decoders.IdentityDecoder
    channel_options = [0, .5, 1]
    options = {"test_size": 1e4, "metric": ['bler', 'ber']}
    simulation = simulations.CustomSimulation(
        encoder=encoder, decoder=decoder, channel=channel)
    results = simulation.simulate([{}], channel_options, code_opt, code_opt, **options)
    results = results[list(results.keys())[0]]
    expected = {(0,): [0, 0], (.5,): [0.45, 0.55], (1,): [1, 1]}
    _assert_results = []
    for snr, metrics in results.items():
        lower, upper = expected[snr]
        _assert_results.append(lower <= metrics['ber'] <= upper)
    assert all(_assert_results)

@pytest.mark.slow
def test_simulation_awgn_polar():
    N, k = 8, 3
    snr = 3
    encoder = encoders.PolarEncoder
    decoder = decoders.PolarDecoder
    modulator = modulators.BpskModulator
    channel = lambda x: channels.BawgnChannel(x, k/N, input_power=1)
    enc_options = {"code_length": N, "info_length": k, "design_channel": "AWGN",
                   "design_channelstate": 0.}
    channel_options = [3, 5, 10]
    sim_var = [{}]
    options = {"metric": ['ber', 'bler'], "test_size": 1e3}
    simulation = simulations.CustomSimulation(
        encoder=encoder, decoder=decoder, modulator=modulator, channel=channel)
    results = simulation.simulate(sim_var, channel_options, enc_options, enc_options, **options)


def test_sim_var_creator():
    sim_var = {"C": [0, 1, 2], "g": [-5, 3]}
    list_sim_var = simulations.create_simulation_var_combinations(sim_var)
    expected = [{"C": 0, "g": -5}, {"C": 1, "g": -5}, {"C": 2, "g": -5},
            {"C": 0, "g": 3}, {"C":1, "g": 3}, {"C": 2, "g": 3}]
    def compare(x, y):
        y = list(y)
        try:
            for elem in x:
                y.remove(elem)
        except ValueError:
            return False
        return not y
    assert compare(list_sim_var, expected)


@pytest.mark.slow
def test_snr_ber_simulation():
    n, k = 16, 8
    test_size = 1000
    design_snr = 0.
    metric = ['ber', 'bler']
    encoder = encoders.PolarEncoder(n, k, "BAWGN", design_snr)
    decoder = decoders.PolarDecoder(n, k, "BAWGN", design_snr)
    channel = channels.BawgnChannel(design_snr, rate=k/n)
    test_snr = range(-5, 6)
    simulation = simulations.ChannelParameterSimulation(
        encoder, decoder, channel, modulator=modulators.BpskModulator())
    results = simulation.simulate(test_snr, test_size)
    assert ((set(results.keys()) == set(test_snr)) and
            (set.union(*[set(k.keys()) for k in results.values()]) == set(metric)))

@pytest.mark.slow
def test_hyperparameter_decoder_simulation_nn():
    n, k = 16, 4
    test_size = 1000
    encoder = encoders.PolarEncoder(n, k, "BAWGN", 0.)
    decoder = decoders.NeuralNetDecoder
    channel = channels.BawgnChannel
    cha_var = {'snr_db': [-5, 0, 5]}
    dec_var = {'layer': ([16, 8], [128]), 'train_snr': (-5, 5)}
    simulation = simulations.HyperparameterSearchDecoderSimulation(
        encoder, decoder, channel, dec_var, cha_var)
    training_opt = {'epochs': 10}
    results = simulation.start_simulation(test_size, training_options=training_opt)

@pytest.mark.slow
def test_hyperparameter_decoder_simulation_svm():
    n, k = 16, 4
    test_size = 1000
    encoder = encoders.PolarWiretapEncoder(n, "BAWGN", "BAWGN", 0., -5.)
    decoder = decoders.SvmDecoder
    channel = channels.BawgnChannel
    cha_var = {'snr_db': [-5, 0, 5]}
    dec_var = {'gamma': (1e-4, 1e4), 'kernel': ('rbf',), 'C': (1, 1e4)}
    simulation = simulations.HyperparameterSearchDecoderSimulation(
        encoder, decoder, channel, dec_var, cha_var)
    results = simulation.start_simulation(test_size)
