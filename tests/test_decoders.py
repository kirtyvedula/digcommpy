import warnings

import numpy as np
import pytest

from digcommpy import decoders
from digcommpy import channels
from digcommpy import modulators, encoders

def test_identity_decoder():
    codewords = np.array([[0, 1], [1, 0], [0, 0], [1, 1]])
    decoder = decoders.IdentityDecoder
    messages = decoder.decode_messages(codewords)
    assert np.all(codewords == messages)

def test_repetition_decoder():
    code_words = np.array([[0, 0, 1], [1, 1, 1], [1, 0, 1]])
    decoder = decoders.RepetitionDecoder()
    _messages = decoder.decode_messages(code_words)
    expected = [[0], [1], [1]]
    assert np.all(_messages == expected)

def test_polar_decoder_awgn_long():
    snr = 0
    channel = channels.BawgnChannel(snr)
    decoder = decoders.PolarDecoder(16, 5, "BAWGN", snr)
    code_words = np.array([[-1, 1, -1, 1, 1, -1, 1, -1, -1, 1, -1, 1, 1, -1, 1, -1],
                           [1, 1, -1, -1, 1, 1, -1, -1, -1, -1, 1, 1, -1, -1, 1, 1]])
    _decoded = decoder.decode_messages(code_words, channel)
    expected = np.array([[0, 1, 0, 1, 0], [1, 0, 1, 0, 1]])
    assert np.all(_decoded == expected)

def test_polar_decoder_awgn():
    snr = 0
    channel = channels.BawgnChannel(snr)
    code_words = np.array([[1, 1, 1, 1], [-1, -1, -1, -1], [1, 1, -1, -1],
                           [-1, -1, 1, 1], [2.5, 2, -.5, -10]])
    decoder = decoders.PolarDecoder(4, 2, "BAWGN", 0)
    _decoded = decoder.decode_messages(code_words, channel)
    expected = np.array([[0, 1], [0, 0], [1, 0], [1, 1], [1, 0]])
    assert np.all(_decoded == expected)


def test_svm_decoder_training_data():
    train_code = np.array([[1, 1, 1, 1], [-1, -1, -1, -1], [1, 1, -1, -1],
                           [-1, -1, 1, 1]])
    train_info_bit = np.array([[0, 1], [0, 0], [1, 0], [1, 1]])
    decoder = decoders.SvmDecoder(4, 2, kernel='linear')
    decoder.train_system([train_code, train_info_bit])
    assert len(decoder.decoder.support_) > 0

def test_svm_decoder_training_encoder():
    enc = encoders.PolarEncoder(4, 2, "BAWGN")
    mod = modulators.BpskModulator()
    decoder = decoders.SvmDecoder(4, 2, C=1, kernel='rbf')
    decoder.train_system([enc, mod])
    assert len(decoder.decoder.support_) > 0


def test_elm_decoder_training_data():
    train_code = np.array([[1, 1, 1, 1], [-1, -1, -1, -1], [1, 1, -1, -1],
                           [-1, -1, 1, 1]])
    train_info_bit = np.array([[0, 1], [0, 0], [1, 0], [1, 1]])
    decoder = decoders.ElmDecoder(4, 2, [(100, 'tanh')])
    decoder.train_system([train_code, train_info_bit])
    assert True

def test_elm_decoder_training_encoder():
    enc = encoders.PolarEncoder(4, 2, "BAWGN")
    mod = modulators.BpskModulator()
    decoder = decoders.ElmDecoder(4, 2, [(100, 'tanh')])
    decoder.train_system([enc, mod])
    assert True


@pytest.mark.parametrize("decoder, parameters",
        [(decoders.SvmDecoder, {'C': 1., 'kernel': 'rbf'}),
         (decoders.ElmDecoder, {'neurons': [(100, 'tanh')]}),
         (decoders.NeuralNetDecoder, {'layer': [50, 20, 10]})])
def test_ml_decoders(decoder, parameters):
    n_test = 1000
    N = 32
    k = 5
    channel = "BAWGN"
    enc = encoders.PolarEncoder(N, k, "BAWGN")
    mod = modulators.BpskModulator()
    decoder = decoder(N, k, **parameters)
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")  # to ignore hpelm warnings
        decoder.train_system([enc, mod])
    code_words = np.random.randint(0, 2, size=(n_test, N))
    code_words = modulators.BpskModulator.modulate_symbols(code_words)
    _decoded = decoder.decode_messages(code_words, channels.BawgnChannel(0))
    assert (np.shape(_decoded) == (n_test, k)) and np.all(np.logical_or(_decoded == 0, _decoded == 1))


def test_polar_wiretap_decoder_pos_lookup():
    n = 8
    snr = 5.
    pos_lookup = np.array([0, -1, -1, -2, 0, -2, -1, -2])
    channel = channels.BawgnChannel(snr)
    code_words = np.array([[-1, 1, -1, 1, -1, -1, -1, -1], [-1, -1, -1, -1, 1, -1, -1, 1]])
    decoder = decoders.PolarWiretapDecoder(n, "BAWGN", snr, pos_lookup=pos_lookup)
    _decoded = decoder.decode_messages(code_words, channel)
    expected = np.array([[0, 1, 0], [1, 1, 1]])
    assert np.all(_decoded == expected)

def test_polar_wiretap_decoder_construction():
    n = 8
    snr_bob = 5.
    snr_eve = 0.
    channel = channels.BawgnChannel(snr_bob)
    code_words = np.array([[-1, 1, -1, 1, -1, -1, -1, -1], [-1, -1, -1, -1, 1, -1, -1, 1]])
    decoder = decoders.PolarWiretapDecoder(n, "BAWGN", "BAWGN", snr_bob, snr_eve)
    _decoded = decoder.decode_messages(code_words, channel)
    expected = np.array([[0, 1, 0], [1, 1, 1]])
    assert np.all(_decoded == expected)
