import functools

import numpy as np
import pytest

from digcommpy import channels


def test_awgn_shape():
    messages = np.array([[1, -1], [-1, -1], [1, 1], [-1, 1], [1, -1]])
    output = channels.BawgnChannel(0.).transmit_data(messages)
    assert np.shape(messages) == np.shape(output)

def test_bec_shape():
    messages = np.array([[1, 0], [0, 0], [1, 1], [0, 1], [1, 0]])
    output = channels.BecChannel(.1).transmit_data(messages)
    assert np.shape(messages) == np.shape(output)

def test_bsc_shape():
    messages = np.array([[1, 0], [0, 0], [1, 1], [0, 1], [1, 0]])
    output = channels.BscChannel(.1).transmit_data(messages)
    assert np.shape(messages) == np.shape(output)

@pytest.mark.parametrize("rate", [-.4, 0., 1.2])
def test_awgn_rate_range(rate):
    with pytest.raises(ValueError):
        channels.BawgnChannel(10., rate=rate)

@pytest.mark.parametrize("epsilon", [-1., 1.2])
def test_bec_epsilon_range(epsilon):
    with pytest.raises(ValueError):
        channels.BecChannel(epsilon=epsilon)

@pytest.mark.parametrize("prob", [-1., 1.2])
def test_bsc_prob_range(prob):
    with pytest.raises(ValueError):
        channels.BscChannel(prob=prob)

def test_bec_output():
    messages = np.random.randint(0, 2, size=(100, 2))
    received = channels.BecChannel(.5).transmit_data(messages)
    zeros = np.isin(received, 0)
    ones = np.isin(received, 1)
    errors = np.isin(received, -1)
    assert np.all(functools.reduce(np.logical_xor, (zeros, ones, errors)))
    
def test_bsc_output():
    messages = np.random.randint(0, 2, size=(100, 2))
    received = channels.BscChannel(.5).transmit_data(messages)
    zeros = np.isin(received, 0)
    ones = np.isin(received, 1)
    assert np.all(functools.reduce(np.logical_xor, (zeros, ones)))


@pytest.mark.parametrize("prob, expected", [(.5, 0), (0, 1)])
def test_bsc_capacity(prob, expected):
    channel = channels.BscChannel(prob)
    capacity = channel.capacity()
    assert capacity == expected

@pytest.mark.parametrize("prob, expected", [(.5, .5), (0, 1), (1, 0)])
def test_bec_capacity(prob, expected):
    channel = channels.BecChannel(prob)
    capacity = channel.capacity()
    assert capacity == expected
