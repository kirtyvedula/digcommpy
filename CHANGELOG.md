# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [0.8] - 2019-07-03
### Added
- Added `channels.Channel.get_channelstate` method to return channelstate
- Added `channels.Channel.name` attribute to channel objects
- Added `info_length_bob` and `random_length` keyword arguments to
  `encoders.PolarWiretapEncoder` and `decoders.PolarWiretapDecoder`
- Added `return_random` keyword to
  `encoders.PolarWiretapEncoder.generate_codebook` to allow returning the
  random bits as a third return value
- Added `info_length_bob` and `random_length` arguments to
  `encoder.PolarWiretapencoder` construction to support fixed rate codes

### Changed
- Changed construction of `encoders.PolarEncoder` to support a channel object
- Changed `decoders.PolarDecoder.decode_messages` to accept a channel object
- Changed order of arguments of `decoders.PolarWiretapDecoder` to match the
  encoder
- The generation of random samples with the
  `information_theory.GaussianMixtureRv` class is faster


## [0.7] - 2019-02-13
### Added
- Added `modulators.QamModulator` and `demodulators.QamDemodulator`

### Changed
- Calculation of `metrics.ber` is only possible on binary sequences

### Fixed
- Greatly improve speed of `encoders.PolarEncoder` and `decoders.PolarDecoder`
- Improve speed of `messages.unpack_to_bits`


## [0.6] - 2018-12-10
### Added
- Added function `parsers.convert_codebook_to_dict` for converting codebooks
  from arrays to a single dict

### Changed
- Changed the general order of random and information bits. Now, the _k_ left
  bits are treated as information (message) bits
- Changed value of `train_snr` in `decoders.NeuralNetDecoder`. It is now the
  Es/N0. If you want to use, e.g. Eb/N0, you have to take the rate into account
  by yourself before training.

## [0.5] - 2018-11-08
### Added
- Added `__len__` method to `information_theory.GaussianMixtureRv` to return
  number of components
- Added `logpdf` method to `information_theory.GaussianMixtureRv` to return the
  log of the pdf
- Added `store_history` keyword to training `decoders.NeuralNetDecoder` to
  store the training history as a pickle file

### Fixed
- Fix bug in `decoders.MachineLearningDecoder.train_system` where not the whole
  codebook was used in the case of wiretap codes
- Fix bug in `encoders.PolarWiretapEncoder.generate_codebook`


## [0.4] - 2018-10-22
### Added
- Added `encoders.CodebookEncoder` class
- Added `parsers.read_codebook_file` function
- Added `parsers.read_hyperparameters_search_results` function
- Added `random_length` to `encoders.Encoder` class for support wiretap codes
- Added `seed` keyword to `simulations.single_simulation` function
- Added `simulations.HyperparameterSearchDecoderSimulation` class

## [0.3] - 2018-10-04
### Added
- Added `generate_codebook` method to `encoders.Encoder` class
- Added `noise_var` attribute to `channels.AwgnChannel` class if input power is
provided upon initialization

### Changed
- The `information_theory.entropy_gauss_mix_upper` and
  `information_theory.entropy_gauss_mix_lower` now accept a float argument for
  sigma.
- Allow passing messages of length _k+r_ to
  `encoders.PolarWiretapEncoder.encode_messages` which treats the indeces 0:r
  as the random bits.

## [0.2] - 2018-09-21
### Added
- Add `entropy_gauss_mix_upper` and `entropy_gauss_mix_lower` functions to
  `information_theory` module
- Add `GaussianMixture` distribution to `information_theory` module
- Add `set_params` method to `channels.Channel` class

### Changed
- Changed structure and inner functionality of `simulations` module

## [0.1] - 2018-08-21
### Added
- Add `message` module
- Add `channels` module
- Add `decoders` module
- Add `encoders` module
- Add `modulators` module
- Add `demodulators` module
- Add `checks` module
- Add `simulations` module
- Add `parsers` module
- Add `metrics` module
- Add `information_theory` module
